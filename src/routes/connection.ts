import {Router} from 'express'

import {createData, deleteData, fetchData, updateData } from '../controller/connection';

const connection = Router();

connection.get('/fetchdata',fetchData)
connection.post('/createdata',createData)
connection.patch('/updatedata/:id',updateData)
connection.delete('/deletedata/:id',deleteData)


export default connection
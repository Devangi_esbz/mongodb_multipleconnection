import {config} from 'dotenv'
import express from 'express';
import cors from 'cors';
import connection from './routes/connection';

config()

const app = express();
const port = 8000;

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
  res.send('Hello wrold!');
});

app.use(connection)

app.listen(port, () => {
 console.log(`app is listening at http://localhost:${port}`);
});

import mongoose from "mongoose";

const dbConnection = (req: any) => {

  mongoose.connection.close()

  mongoose.connect(`mongodb://localhost:27017/${req.headers['database_name']}`, {
    autoCreate: true,
    connectTimeoutMS: 10000,
    socketTimeoutMS: 45000,
    maxPoolSize: 10,
    monitorCommands: true,
  });

  mongoose.connection.on("connected", () => {
    console.log("Mongoose default connection open to " + `mongodb://localhost:27017/${req.headers['database_name']}`);
  });

  mongoose.connection.on("error", (err: any) => {
    console.log("Mongoose default connection error: " + err);
  });

  mongoose.connection.on("disconnected", () => {
    console.log("Mongoose default connection disconnected");
  });
};

export default dbConnection;

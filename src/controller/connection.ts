import { Request, Response } from "express";
import { User } from "../models/user";
import generalResponse from "../helper/generalResponse";
import dbConnection from "../db/config";
import mongoose from "mongoose";

export const fetchData = async (req: Request, res: Response) => {
  try {
    dbConnection(req);
    const data = await User.find({});
    return generalResponse(
      res,
      { data },
      "You have successfully fetch the data .",
      "success",
      true,
      201
    );
  } catch (error) {
    return generalResponse(res, error, "", "error", false, 400);
  }
};

export const createData = async (req: Request, res: Response) => {
  try {
    dbConnection(req);
    const bodyData = req.body;
    const user = await User.create(bodyData);
    return generalResponse(
      res,
      { user: user },
      "You have successfully created data .",
      "success",
      true,
      201
    );
  } catch (error) {
    return generalResponse(res, error, "", "error", false, 400);
  }
};

export const updateData = async (req: Request, res: Response) => {
  try {
    dbConnection(req);
    const bodyData = req.body;
    const id = new mongoose.Types.ObjectId(req.params.id);

    const data = await User.findOneAndUpdate({ _id: id }, bodyData, {
      new: true,
    });
    return generalResponse(
      res,
      { data },
      "You have successfully updated data .",
      "success",
      true,
      201
    );
  } catch (error) {
    return generalResponse(res, error, "", "error", false, 400);
  }
};

export const deleteData = async (req: Request, res: Response) => {
  try {
    dbConnection(req);
    const id = new mongoose.Types.ObjectId(req.params.id);

   await User.findOneAndDelete({ _id: id });

    const data = await User.find({})
    return generalResponse(
      res,
      { data},
      "You have successfully deleted data .",
      "success",
      true,
      201
    );
  } catch (error) {
    return generalResponse(res, error, "", "error", false, 400);
  }
};

import { Schema, model } from 'mongoose'
import mongoose from 'mongoose'

export interface UserAttribute {
  name: string
  username: string
  email: string
  password: string
  contactNumber: string
}

export interface ExtentedUserAttributes extends UserAttribute {
  _id: mongoose.Types.ObjectId
}

const schema = new Schema<UserAttribute>(
  {
    name: {
      type: String,
      required: true,
      minlength: 3,
    },
    username: {
      type: String,
      required: true,
      minlength: 3,
      // unique: true,
    },
    email: {
      type: String,
      required: true,
      // immutable: true,
      // unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    contactNumber: {
      type: String,
      required: true,
      // unique: true,
    },
  },
  {
    timestamps: true,
  },
)

export const User = model<UserAttribute>('user', schema)
